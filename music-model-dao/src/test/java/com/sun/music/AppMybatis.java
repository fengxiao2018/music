package com.sun.music;

import org.mybatis.generator.api.ShellRunner;

public class AppMybatis {

    public static void main(String[] args) {
        args = new String[] { "-configfile", "src\\test\\resources\\mybatis-generator-config.xml", "-overwrite" };
        ShellRunner.main(args);
    }

}
