package com.sun.spring.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@PropertySource(value = { "classpath:jdbc.properties"}, ignoreResourceNotFound = true)

public class JdbcConfig {
	
	@Value("${jdbc.url}")
    private String jdbcUrl;

    @Value("${jdbc.driverClassName}")
    private String jdbcDriverClassName;

    @Value("${jdbc.username}")
    private String jdbcUsername;

    @Value("${jdbc.password}")
    private String jdbcPassword;

    @Bean(destroyMethod = "close")
    public DataSource dataSource() {
    	DruidDataSource dataSource = new DruidDataSource();
        // 数据库驱动
    	dataSource.setDriverClassName(jdbcDriverClassName);
        // 相应驱动的jdbcUrl
    	dataSource.setUrl(jdbcUrl);
        // 数据库的用户名
    	dataSource.setUsername(jdbcUsername);
        // 数据库的密码
    	dataSource.setPassword(jdbcUsername);
    	
    	dataSource.setMaxActive(20);
    	dataSource.setInitialSize(1);
    	dataSource.setMinIdle(3);
    	dataSource.setMaxWait(60000);
    	dataSource.setTimeBetweenEvictionRunsMillis(60000);
    	dataSource.setMinEvictableIdleTimeMillis(300000);
    	dataSource.setTestWhileIdle(true);
    	dataSource.setTestOnBorrow(false);
    	dataSource.setTestOnReturn(false);
    	dataSource.setPoolPreparedStatements(true);
        return dataSource;
}

}
