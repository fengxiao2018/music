package com.sun.model.beans.result;

import com.sun.model.beans.excep.MusicRuntimeException;
import com.sun.model.beans.inter.SystemInter;

/**
 * <p>
 * Title: ResultExceptionBean
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author max
 * @date 2018年5月11日 上午12:09:30
 */
public class ResultExceptionBean extends ResultBean {
	/** serialVersionUID */
	private static final long serialVersionUID = 6545355891570931243L;

	public ResultExceptionBean() {
		setState("500");
	}

	public ResultExceptionBean(Exception exception) {
		setState("500");
		if (exception instanceof MusicRuntimeException) {
			setMsg(exception.getMessage());
		}else {
			setMsg(SystemInter.EXCEPTIONMSG);
		}
	}

}
