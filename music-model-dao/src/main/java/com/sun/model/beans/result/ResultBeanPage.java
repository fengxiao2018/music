package com.sun.model.beans.result;

import com.alibaba.fastjson.JSON;

/**
 * <p>
 * Title: ResultBeanPage
 * </p>
 * <p>
 * Description: 
 * </p>
 * 
 * @author max
 * @date 2018年4月30日 下午6:43:28
 */
public class ResultBeanPage extends ResultBean {
	private static final long serialVersionUID = 1L;

	public Page page;

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public ResultBeanPage() {
		super();
	}

	public ResultBeanPage(String state, String msg) {
		super(state, msg);
	}

	public ResultBeanPage(Object data) {
		super(data);
	}

	public ResultBeanPage(Object data, Page page) {
		super(data);
		setPage(page);
	}
	
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
