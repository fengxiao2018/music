package com.sun.model.beans.result;

import com.alibaba.fastjson.JSON;
import com.sun.model.beans.Bean;
import com.sun.util.date.DateUtil;

public class ResultBean extends Bean {
	private static final long serialVersionUID = 1L;

	protected String state = "200";

	protected Object data;

	protected String msg="请求成功！";

	protected String time = DateUtil.getNewDateTimeStr();

	public ResultBean() {
		super();
	}

	public ResultBean(Object data) {
		super();
		this.data = data;
	}

	public ResultBean(Object data, String msg) {
		super();
		this.data = data;
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public ResultBean(String state, String msg) {
		super();
		this.state = state;
		this.msg = msg;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}

}
