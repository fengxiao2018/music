package com.sun.model.beans.excep;

/**
 * <p>
 * Title: MusicRuntimeException
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author max
 * @date 2018年5月11日 上午12:15:32
 */
public class MusicRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MusicRuntimeException() {
		super();
	}

	public MusicRuntimeException(String arg0) {
		super(arg0);
	}

	public MusicRuntimeException(Exception arg1) {
		super(arg1);
	}

	public MusicRuntimeException(String arg0, Exception arg1) {
		super(arg0, arg1);
	}

}
