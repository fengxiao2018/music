package com.sun.model.beans.result;

import com.alibaba.fastjson.JSON;

/**
 * <p>
 * Title: Page
 * </p>
 * <p>
 * Description: 分页工具
 * </p>
 * 
 * @author max
 * @date 2018年4月30日 下午6:15:02
 */
public class Page {

	/**
	 * 每页显示条数
	 */
	public Integer pageSize;

	/**
	 * 当前页数
	 */
	public Integer pageNum;

	/**
	 * 总条数
	 */
	public Integer totalSize;

	/**
	 * 总页数
	 */
	public Integer totalNum;

	/**
	 * 排序
	 */
	public String sort;

	public Page() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * <p>
	 * Title:
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNum
	 *            当前页数
	 */
	public Page(Integer pageSize, Integer pageNum) {
		super();
		this.pageSize = pageSize;
		this.pageNum = pageNum;
	}

	/**
	 * 
	 * <p>
	 * Title:
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNum
	 *            当前页数
	 * @param totalSize
	 *            总条数
	 */
	public Page(Integer pageSize, Integer pageNum, Integer totalSize) {
		super();
		this.pageSize = pageSize;
		this.pageNum = pageNum;
		this.totalSize = totalSize;
		this.totalNum = totalSize % pageSize == 0 ? totalSize / pageSize : totalSize / pageSize + 1;
	}

	public Page(Integer pageSize, Integer pageNum, Integer totalSize, Integer totalNum) {
		super();
		this.pageSize = pageSize;
		this.pageNum = pageNum;
		this.totalSize = totalSize;
		this.totalNum = totalNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
		this.totalNum = totalSize % pageSize == 0 ? totalSize / pageSize : totalSize / pageSize + 1;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
	

}
