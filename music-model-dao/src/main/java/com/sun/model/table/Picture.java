package com.sun.model.table;

import java.io.Serializable;

public class Picture implements Serializable {
    private Integer id;

    private String picName;

    private Long addTime;

    private String addUser;

    private Integer temperature;

    private String picCategory;

    private String bery;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName == null ? null : picName.trim();
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser == null ? null : addUser.trim();
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getPicCategory() {
        return picCategory;
    }

    public void setPicCategory(String picCategory) {
        this.picCategory = picCategory == null ? null : picCategory.trim();
    }

    public String getBery() {
        return bery;
    }

    public void setBery(String bery) {
        this.bery = bery == null ? null : bery.trim();
    }
}