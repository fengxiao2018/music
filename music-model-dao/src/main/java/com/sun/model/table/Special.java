package com.sun.model.table;

import java.io.Serializable;
import java.util.Date;

public class Special implements Serializable {
    private Integer specialId;

    private Integer singerId;

    private String specialName;

    private String specialHead;

    private Integer specialHot;

    private Integer spHouse;

    private Date spAddTime;

    private static final long serialVersionUID = 1L;

    public Integer getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Integer specialId) {
        this.specialId = specialId;
    }

    public Integer getSingerId() {
        return singerId;
    }

    public void setSingerId(Integer singerId) {
        this.singerId = singerId;
    }

    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName == null ? null : specialName.trim();
    }

    public String getSpecialHead() {
        return specialHead;
    }

    public void setSpecialHead(String specialHead) {
        this.specialHead = specialHead == null ? null : specialHead.trim();
    }

    public Integer getSpecialHot() {
        return specialHot;
    }

    public void setSpecialHot(Integer specialHot) {
        this.specialHot = specialHot;
    }

    public Integer getSpHouse() {
        return spHouse;
    }

    public void setSpHouse(Integer spHouse) {
        this.spHouse = spHouse;
    }

    public Date getSpAddTime() {
        return spAddTime;
    }

    public void setSpAddTime(Date spAddTime) {
        this.spAddTime = spAddTime;
    }
}