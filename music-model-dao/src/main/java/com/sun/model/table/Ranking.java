package com.sun.model.table;

import java.io.Serializable;

public class Ranking implements Serializable {
    private Integer rankingId;

    private String rankingName;

    private String abbreviationName;

    private Long addTime;

    private String rankingPic;

    private Integer hot;

    private String beiy;

    private static final long serialVersionUID = 1L;

    public Integer getRankingId() {
        return rankingId;
    }

    public void setRankingId(Integer rankingId) {
        this.rankingId = rankingId;
    }

    public String getRankingName() {
        return rankingName;
    }

    public void setRankingName(String rankingName) {
        this.rankingName = rankingName == null ? null : rankingName.trim();
    }

    public String getAbbreviationName() {
        return abbreviationName;
    }

    public void setAbbreviationName(String abbreviationName) {
        this.abbreviationName = abbreviationName == null ? null : abbreviationName.trim();
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public String getRankingPic() {
        return rankingPic;
    }

    public void setRankingPic(String rankingPic) {
        this.rankingPic = rankingPic == null ? null : rankingPic.trim();
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public String getBeiy() {
        return beiy;
    }

    public void setBeiy(String beiy) {
        this.beiy = beiy == null ? null : beiy.trim();
    }
}