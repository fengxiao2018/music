package com.sun.model.table;

import java.io.Serializable;
import java.util.Date;

public class Song implements Serializable {
    private Integer songId;

    private Integer specialId;

    private Integer singerId;

    private String songName;

    private Date publishtime;

    private String songHead;

    private String mp3;

    private String mv;

    private Integer isvipsong;

    private Integer mSign;

    private Integer randkingId;

    private static final long serialVersionUID = 1L;

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public Integer getSpecialId() {
        return specialId;
    }

    public void setSpecialId(Integer specialId) {
        this.specialId = specialId;
    }

    public Integer getSingerId() {
        return singerId;
    }

    public void setSingerId(Integer singerId) {
        this.singerId = singerId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName == null ? null : songName.trim();
    }

    public Date getPublishtime() {
        return publishtime;
    }

    public void setPublishtime(Date publishtime) {
        this.publishtime = publishtime;
    }

    public String getSongHead() {
        return songHead;
    }

    public void setSongHead(String songHead) {
        this.songHead = songHead == null ? null : songHead.trim();
    }

    public String getMp3() {
        return mp3;
    }

    public void setMp3(String mp3) {
        this.mp3 = mp3 == null ? null : mp3.trim();
    }

    public String getMv() {
        return mv;
    }

    public void setMv(String mv) {
        this.mv = mv == null ? null : mv.trim();
    }

    public Integer getIsvipsong() {
        return isvipsong;
    }

    public void setIsvipsong(Integer isvipsong) {
        this.isvipsong = isvipsong;
    }

    public Integer getmSign() {
        return mSign;
    }

    public void setmSign(Integer mSign) {
        this.mSign = mSign;
    }

    public Integer getRandkingId() {
        return randkingId;
    }

    public void setRandkingId(Integer randkingId) {
        this.randkingId = randkingId;
    }
}