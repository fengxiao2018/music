package com.sun.model.table;

import java.io.Serializable;
import java.util.Date;

public class Estimate implements Serializable {
    private Integer estimateId;

    private Integer songId;

    private Integer userId;

    private Date addTime;

    private String estimateInfo;

    private static final long serialVersionUID = 1L;

    public Integer getEstimateId() {
        return estimateId;
    }

    public void setEstimateId(Integer estimateId) {
        this.estimateId = estimateId;
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getEstimateInfo() {
        return estimateInfo;
    }

    public void setEstimateInfo(String estimateInfo) {
        this.estimateInfo = estimateInfo == null ? null : estimateInfo.trim();
    }
}