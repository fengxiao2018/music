package com.sun.dao;

import java.util.List;

import com.sun.model.table.Estimate;

public interface EstimateMapper {
    int deleteByPrimaryKey(Integer estimateId);

    int insert(Estimate record);

    int insertSelective(Estimate record);

    Estimate selectByPrimaryKey(Integer estimateId);

    int updateByPrimaryKeySelective(Estimate record);

    int updateByPrimaryKeyWithBLOBs(Estimate record);

    int updateByPrimaryKey(Estimate record);

	List<Estimate> findBySong(Integer song_id);
}