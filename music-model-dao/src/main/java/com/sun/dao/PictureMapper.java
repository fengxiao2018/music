package com.sun.dao;

import java.util.List;
import java.util.Map;

import com.sun.model.table.Picture;

public interface PictureMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Picture record);

    int insertSelective(Picture record);

    Picture selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Picture record);

    int updateByPrimaryKey(Picture record);

	Picture findByNameAndCategory(String picName, String picCategory);

	List<Picture> findByCategory(Map<String, Object> prams, String pic_category, int i, Integer picnum);
}