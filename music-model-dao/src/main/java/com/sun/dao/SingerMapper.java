package com.sun.dao;

import java.util.List;

import com.sun.model.table.Singer;

public interface SingerMapper {
    int deleteByPrimaryKey(Integer singerId);

    int insert(Singer record);

    int insertSelective(Singer record);

    Singer selectByPrimaryKey(Integer singerId);

    int updateByPrimaryKeySelective(Singer record);

    int updateByPrimaryKeyWithBLOBs(Singer record);

    int updateByPrimaryKey(Singer record);

	List<Singer> findSinger(int i, Integer songmainnum);

	Singer findSingerByAll(Singer singer);

}