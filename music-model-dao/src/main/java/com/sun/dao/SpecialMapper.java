package com.sun.dao;

import java.util.List;
import java.util.Map;

import com.sun.model.table.Special;

public interface SpecialMapper {
    int deleteByPrimaryKey(Integer specialId);

    int insert(Special record);

    int insertSelective(Special record);

    Special selectByPrimaryKey(Integer specialId);

    int updateByPrimaryKeySelective(Special record);

    int updateByPrimaryKey(Special record);

	List<Special> findSpecialBy(Map<String, Object> params, int i, Integer specialnum, Integer singer_id);
}