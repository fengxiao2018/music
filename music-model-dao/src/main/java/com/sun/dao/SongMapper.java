package com.sun.dao;

import java.util.List;
import java.util.Map;

import com.sun.model.table.Song;

public interface SongMapper {
    int deleteByPrimaryKey(Integer songId);

    int insert(Song record);

    int insertSelective(Song record);

    Song selectByPrimaryKey(Integer songId);

    int updateByPrimaryKeySelective(Song record);

    int updateByPrimaryKey(Song record);

	List<Song> findSpecialSong(Integer specialId);

	List<Song> findSongMVHot(int i, Integer songmainnum);

	Song findSongMVByPage(Integer page, int i);

	List<Song> findSongBySort(Map<String, Object> params, int i, Integer sortnum);

	Integer findpageBySort(String nation, Integer mSign);

	List<Song> findSongBySingerId(Integer singerId);

	List<Song> findSongNew(int i, Integer songmainnum);

	Integer findSongNumPage(Integer mSign);
}