package com.sun.dao;

import com.sun.model.table.Ranking;

public interface RankingMapper {
    int deleteByPrimaryKey(Integer rankingId);

    int insert(Ranking record);

    int insertSelective(Ranking record);

    Ranking selectByPrimaryKey(Integer rankingId);

    int updateByPrimaryKeySelective(Ranking record);

    int updateByPrimaryKey(Ranking record);
}