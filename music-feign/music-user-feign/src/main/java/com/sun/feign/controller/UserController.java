package com.sun.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sun.feign.inter.UserInter;
import com.sun.model.beans.result.ResultBean;

@RestController
public class UserController {

	@Autowired
	private UserInter userInter;
	
	@RequestMapping("test")
	public ResultBean test() {
		return userInter.test();
	}
}
