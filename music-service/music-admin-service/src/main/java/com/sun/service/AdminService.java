package com.sun.service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sun.dao.SingerMapper;
import com.sun.dao.UserMapper;
import com.sun.lib.Tool;
import com.sun.model.table.Singer;
import com.sun.model.table.Song;
import com.sun.model.table.User;

@Service
public class AdminService extends FathService {

	@Resource
	private UserMapper userMapper;

	@Resource
	private SingerMapper singerMapper;

	/**
	 * 管理员用户登陆验证
	 * 
	 * @param adminname
	 * @param adminpwd
	 * @return
	 */
	public Map<String, Object> checkadminlogin(String adminname, String adminpwd) {
		if (adminname == null || adminpwd == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		User user = userMapper.findByUserName(adminname);
		if (user == null) {
			map.put(FLAG, NAMEERROR);
		} else if (Tool.comparePwd(adminpwd, user.getPassword())) {
			if (WHETHERNO.equals(user.getIslogin())
					&& WHETHERNO.equals(user.getIsfrozen())
					&& WHETHERYSE.equals(user.getIsadmin())) {
				// user.setIsLogin(WHETHERYSE);
				// userMapper.update(user);
				map.put(FLAG, SUCCESS);
				map.put(RET, user);
			} else if (WHETHERNO.equals(user.getIsadmin())) {
				map.put(FLAG, ISNOTADMIN);
			} else if (WHETHERYSE.equals(user.getIsfrozen())) {
				map.put(FLAG, ISFROZEN);
			} else if (WHETHERYSE.equals(user.getIslogin())) {
				map.put(FLAG, ISLOGIN);
			}
		} else {
			map.put(FLAG, PWDERROR);
		}
		return map;
	}

	/**
	 * 添加歌手
	 * 
	 * @param singer
	 * @return
	 */
	public Map<String, Object> addSinger(Singer singer) {
		if (singer == null) {
			throw new RuntimeException(ERRMSG);
		}
		if ("".equals(singer.getSingerName())) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		singer.setNation(Tool.dellNull(singer.getNation()));
		singer.setSingerHead(Tool.dellNull(singer.getSingerHead()));
		singer.setSex(Tool.dellNull(singer.getSex()));
		singer.setSingerInfo(Tool.dellNull(singer.getSingerInfo()));
		/**
		 * 查询是否相同
		 */
		Singer singer1 = singerMapper.findSingerByAll(singer);
		if (singer1 == null) {
			singerMapper.insert(singer);
			map.put(FLAG, SUCCESS);
		} else {
			map.put(FLAG, "歌手已存在！");
		}
		return map;
	}

	/**
	 * 上传MP3文件，并写入硬盘中
	 * @param file
	 * @param request
	 * @param flag mv或mp3
	 * @return
	 * @throws IOException
	 */
	public Map<String, Object> adminAddmp3mv(MultipartFile file,
			HttpServletRequest request,String flag) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		if (!file.isEmpty()) {
			ServletContext sc = request.getSession().getServletContext();
			String dir = sc.getRealPath("/"+flag); // 设定文件保存的目录
			String mvmp3 = file.getOriginalFilename(); // 得到上传时的文件名
			 if (mvmp3.split(".").length>=2) {
				flag=mvmp3.split(".")[mvmp3.split(".").length-1];
				mvmp3=mvmp3.substring(0, mvmp3.lastIndexOf("."));
			}
			/**
			 * 取随机的文件名。
			 */
			String filename = Tool.getRandomName() + "."+flag;
			FileUtils.writeByteArrayToFile(new File(dir, filename),
					file.getBytes());
			Song song=new Song();
			if (MP3.equals(flag)) {
				song.setMp3(filename);
			}else if (MV.equals(flag)) {
				song.setMv(filename);
			}else{}
			song.setSongName(mvmp3);
			map.put(SONG, song);
			map.put(FLAG, SUCCESS);
		}else{
			map.put(FLAG, "文件上传失敗！");
		}
		return map;
	}

}
