package com.sun.web;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sun.lib.Tool;
import com.sun.model.beans.result.ResultBean;
import com.sun.model.table.Singer;
import com.sun.model.table.Song;
import com.sun.model.table.User;
import com.sun.service.AdminService;
import com.sun.service.SongService;

@RestController
@RequestMapping("/admin")
public class AdminController extends FathController{



	@Resource
	private AdminService adminService;

	@Resource
	private SongService songService;

	/**
	 * 进入admin登陆界面
	 * 
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAdminLogin.do")
	public String toAdminLogin(HttpSession session, Model model) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
			return "admin/adminmainpage";
		}
		return "admin/adminlogin";
	}

	/**
	 * 进入admin登出界面
	 * 
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("/toAdminLoginOut.do")
	public String toAdminLoginOut(HttpSession session, Model model) {
		session.invalidate();
		return "admin/adminlogin";
	}

	/**
	 * admin登陆
	 * 
	 * @param session
	 * @param model
	 * @param adminname
	 * @param adminpwd
	 * @return
	 */
	@RequestMapping("/adminloginfrom.do")
	public String adminloginfrom(HttpSession session, Model model,
			String adminname, String adminpwd) {
		Map<String, Object> map = adminService.checkadminlogin(adminname,
				adminpwd);
		if (SUCCESS.equals(Tool.dellNull(map.get(FLAG)))) {
			session.setAttribute(USER, (User) map.get(RET));
			model.addAttribute(USER, (User) map.get(RET));
			return "admin/adminmainpage";
		} else if (NAMEERROR.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute(MSG, "用户名不存在！");
		} else if (PWDERROR.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute(MSG, "密码错误！");
		} else if (ISLOGIN.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute(MSG, "用户已登陆！");
		} else if (ISFROZEN.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute(MSG, "用户已冻结！");
		} else if (ISNOTADMIN.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute(MSG, "用户不是管理员！");
		}
		return "admin/adminlogin";
	}

	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/modifyUser.do")
	public String modifyUser(HttpSession session, String user_id) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			return "admin/modifyUser";
		}
		return "admin/adminmainpage";
	}

	/**
	 * 
	 * @param session
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/FirstFindSingerByPage.do")
	public String firstFindSingerByPage(HttpSession session, String user_id) {

		return "admin/adminFindSingerByPage";
	}

	/**
	 * 
	 * @param session
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/toadminAddSinger.do")
	public String toadminAddSinger(String user_id) {

		return "admin/adminAddSinger";
	}

	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/adminAddSinger.do")
	public String adminAddSinger(Singer singer, Model model) {
		Map<String, Object> retMap = adminService.addSinger(singer);
		if (SUCCESS.equals(Tool.dellNull(retMap.get(FLAG)))) {
			return "admin/adminFindSingerByPage";
		}
		model.addAttribute("singermsg", Tool.dellNull(retMap.get(FLAG)));
		return "admin/adminAddSinger";
	}

	/**
	 * 歌手的歌曲信息
	 * 
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/toFirstFindSongBySingerByPage.do")
	public String toFirstFindSongBySingerByPage(Integer singer_id, Model model) {
		model.addAttribute("singer_id", singer_id);
		return "admin/adminFindSongBySingerByPage";
	}

	/**
	 * 加载添加MP3界面
	 * 
	 * @return
	 */
	@RequestMapping("/toadminAddmp3.do")
	public String toadminAddmp3(Integer singer_id,String singer_name,Model model) {
		Singer singer=new Singer();
		if (singer_id!=null&&singer_name!=null) {
			singer.setSingerId(singer_id);
			singer.setSingerName(singer_name);
		}
		model.addAttribute("singer", singer);
		return "admin/adminAddmp3";
	}

	/**
	 * 加载添加MV界面
	 * 
	 * @return
	 */
	@RequestMapping("/toadminAddmv.do")
	public String toadminAddmv(Integer singer_id,String singer_name,Model model) {
		Singer singer=new Singer();
		if (singer_id!=null&&singer_name!=null) {
			singer.setSingerId(singer_id);
			singer.setSingerName(singer_name);
		}
		model.addAttribute("singer", singer);
		return "admin/adminAddmv";
	}

	/**
	 * 上传MP3文件
	 * 
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/adminAddmp31.do")
	public ResultBean adminAddmp31(MultipartFile mp3file, HttpServletRequest request)
			throws IOException {
		Map<String, Object> retMap = adminService.adminAddmp3mv(mp3file,
				request, MP3);
		ResultBean re = new ResultBean();
		if (SUCCESS.equals(Tool.dellNull(retMap.get(FLAG)))) {
			re.setData(retMap.get(SONG));
			re.setMsg("文件上传成功！");
		} else {
			re.setData(Tool.dellNull(retMap.get(FLAG)));
		}
		return re;
	}

	/**
	 * 
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/adminAddmp32.do")
	public String adminAddmp32(Song song, Model model) {
		Map<String, Object> retMap = songService.saveSong(song);
		if (TRUE.equals(Tool.dellNull(retMap.get(FLAG)))) {
			return "admin/adminFindSingerByPage";
		}
		model.addAttribute("massage", Tool.dellNull(retMap.get(FLAG)));
		return "admin/adminAddmp3";
	}
	
	/**
	 * 上传Mv文件
	 * 
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("/adminAddmv1.do")
	public ResultBean adminAddmv1(MultipartFile mvfile, HttpServletRequest request)
			throws IOException {
		Map<String, Object> retMap = adminService.adminAddmp3mv(mvfile,
				request, MV);
		ResultBean re = new ResultBean();
		if (SUCCESS.equals(Tool.dellNull(retMap.get(FLAG)))) {
			re.setData(retMap.get(SONG));
			re.setMsg("文件上传成功！");
		} else {
			re.setData(Tool.dellNull(retMap.get(FLAG)));
		}
		return re;
	}

	/**
	 * 
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/adminAddmv2.do")
	public String adminAddmv2(Song song, Model model) {
		Map<String, Object> retMap = songService.saveSong(song);
		if (TRUE.equals(Tool.dellNull(retMap.get(FLAG)))) {
			return "admin/adminFindSingerByPage";
		}
		model.addAttribute("massage", Tool.dellNull(retMap.get(FLAG)));
		return "admin/adminAddmp3";
	}
	
	/**
	 * 
	 * @param session
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/toadminFindAllSpecial.do")
	public String toadminFindAllSpecial(String user_id) {

		return "admin/adminFindAllSpecial";
	}
	
	/**
	 * 
	 * @param session
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/toadminAddSpecial.do")
	public String toadminAddSpecial(String user_id) {

		return "admin/adminAddSpecial";
	}
	
	/**
	 * 
	 * @param session
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/adminAddSpecial.do")
	public String adminAddSpecial(String user_id) {

		return "admin/adminFindSingerByPage";
	}
	
}

