package com.sun.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.lib.Tool;
import com.sun.model.beans.result.ResultBean;
import com.sun.model.beans.result.ResultBeanPage;
import com.sun.model.table.Singer;
import com.sun.model.table.Song;
import com.sun.model.table.User;
import com.sun.service.MainService;

/**
 * 主页控制层
 * 
 * @author maxiao
 * 
 */
@RestController
@RequestMapping("/main")
public class MainController extends FathController {

	@Autowired
	private MainService mainService;

	/**
	 * 加载我的音乐
	 * 
	 * @return
	 */
	@RequestMapping("/tomainPage.do")
	public String tomainPage(Model model, HttpSession session) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		return "page/mainPage";
	}

	/**
	 * 跳转top页面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/totop.do")
	public String totop(Model model, HttpSession session) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		return "page/sort";
	}

	/**
	 * 跳转special页面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/tospecial.do")
	public String tospecial(Model model, HttpSession session) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		return "page/special";
	}

	/**
	 * 跳转mv页面
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/tomv.do")
	public String tomv(Model model, HttpSession session) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		return "page/mv";
	}

	/**
	 * 加载我的音乐
	 * 
	 * @return
	 */
	@RequestMapping("/tomymusic.do")
	public String tomymusic(HttpSession session, Model model) {
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		return "page/myMusic";
	}

	/**
	 * 查看歌手信息
	 * 
	 * @param singer_id
	 * @return
	 */
	@RequestMapping("/tosinger.do")
	public String tosinger(Integer singer_id, Model model, HttpSession session) {
		Map<String, Object> map = mainService.fingSingerById(singer_id);
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute("singer", (Singer) map.get(RET));
			return "page/singer";
		}
		return "page/mainPage";
	}

	/**
	 * 查看歌曲信息
	 * 
	 * @param song_id
	 * @return
	 */
	@RequestMapping("/tosong.do")
	public String tosong(Integer song_id, Model model, HttpSession session) {
		Map<String, Object> map = mainService.findSongById(song_id);
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute("song", (Song) map.get(RET));
			return "page/myMusic";
		}
		return "page/singer";
	}

	/**
	 * 加载主页面歌手
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addMainSinger.do")
	public ResultBeanPage addMainSinger() {
		Map<String, Object> map = mainService.findSinger();
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Singer>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载歌手的歌曲
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addSongBySinger.do")
	public ResultBeanPage addSongBySinger(Integer singer_id) {
		Map<String, Object> map = mainService.findSongBySingerId(singer_id);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载我的音乐 的歌
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addsong.do")
	public ResultBeanPage addsong(Integer page) {
		Map<String, Object> map = mainService.findSongNew(page);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载音乐的页数
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addpageChange.do")
	public ResultBean addpageChange() {
		Map<String, Object> map = mainService.findSongNumPage(SONGMP3);
		ResultBean re = new ResultBean();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Integer) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载音乐的页数
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/findSongById.do")
	public ResultBean findSongById(Integer song_id) {
		Map<String, Object> map = mainService.findSongById(song_id);
		ResultBean re = new ResultBean();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Song) map.get(RET));
		}
		return re;
	}
}
