package com.sun.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.lib.Tool;
import com.sun.model.beans.result.ResultBean;
import com.sun.model.beans.result.ResultBeanPage;
import com.sun.model.table.Estimate;
import com.sun.model.table.Song;
import com.sun.model.table.User;
import com.sun.service.MainService;
import com.sun.service.SongService;
import com.sun.service.SpecialService;

/**
 * 歌曲控制层
 * 
 * @author maxiao
 * 
 */
@RestController
@RequestMapping("/song")
public class SongController extends FathController {

	@Autowired
	private SongService songService;
	@Autowired
	private MainService mainService;
	@Autowired
	private SpecialService specialService;

	/**
	 * 加载mv
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addsongMV.do")
	public ResultBeanPage addsongMV(Integer page) {
		Map<String, Object> map = songService.findSongMVHot(page);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载MV的页数
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addpageMVChange.do")
	public ResultBean addpageMVChange() {
		Map<String, Object> map = mainService.findSongNumPage(SONGMV);
		ResultBean re = new ResultBean();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Integer) map.get(RET));
		}
		return re;
	}

	/**
	 * 跳转mv页面
	 * 
	 * @param song_id
	 * @param model
	 * @return
	 */
	@RequestMapping("/toplayer.do")
	public String toplayer(Integer song_id, Model model, HttpSession session) {
		Map<String, Object> map = mainService.findSongById(song_id);
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute("song", (Song) map.get(RET));
			return "page/player";
		}
		return "page/mymusic";
	}

	/**
	 * 加载浮动图片
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addshowPic.do")
	public ResultBeanPage addshowPic(Integer page) {
		Map<String, Object> map = songService.findSongMVByPage(page);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Song) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载歌曲评价
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addEstimateBySong.do")
	public ResultBeanPage addEstimateBySong(Integer song_id) {
		Map<String, Object> map = songService.findEstimateBySong(song_id);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Estimate>) map.get(RET));
		}
		return re;
	}

	/**
	 * 发表评价
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addEstimate.do")
	public ResultBeanPage addEstimate(Integer song_id, String textarea,
			HttpSession session) {
		User user = (User) session.getAttribute(USER);
		Map<String, Object> map = songService.addEstimate(song_id, textarea,
				user.getUserId());
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Estimate) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载专辑歌曲
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addSpecialSong.do")
	public ResultBeanPage addSpecialSong(Integer special_id) {
		Map<String, Object> map = songService.addSpecialSong(special_id);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载专辑
	 * 
	 * @param page
	 * @param flag
	 * @param singer_id
	 *            歌手id，送0或空时表示全部
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/findSpecial.do")
	public ResultBeanPage findSpecial(Integer page, Integer flag, Integer singer_id) {
		Map<String, Object> map = specialService.findSpecial(page, flag,
				singer_id);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载排行榜歌曲
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addSongBySort.do")
	public ResultBeanPage addSongBySort(Integer page, Integer flag) {
		Map<String, Object> map = songService.addSongBySort(page, flag);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}

	/**
	 * 加载排行榜音乐的页数
	 * 
	 * @param page
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/addpageBySort.do")
	public ResultBean addpageBySort(Integer flag) {
		Map<String, Object> map = songService.findpageBySort(flag, SONGMP3);
		ResultBean re = new ResultBean();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((Integer) map.get(RET));
		}
		return re;
	}
}
