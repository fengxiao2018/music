package com.sun.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.lib.Tool;
import com.sun.model.beans.result.ResultBeanPage;
import com.sun.model.table.Song;
import com.sun.service.SpecialService;

/**
 * 
 * @author maxiao
 * 
 */
@RestController
@RequestMapping("/special")
public class SpecialController extends FathController {

	@Autowired
	private SpecialService specialService;

	/**
	 * 加载专辑
	 * 
	 * @param page
	 * @param flag
	 * @param singer_id
	 *            歌手id，送0或空时表示全部
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/findSpecial.do")
	public ResultBeanPage findSpecial(Integer page, Integer flag, Integer singer_id,
			Integer specialnum) {
		Map<String, Object> map = specialService.findSpecial(page, flag,
				singer_id, specialnum);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Song>) map.get(RET));
		}
		return re;
	}
}
