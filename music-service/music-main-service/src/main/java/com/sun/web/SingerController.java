package com.sun.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sun.lib.Tool;
import com.sun.model.beans.result.ResultBeanPage;
import com.sun.model.table.Singer;
import com.sun.model.table.User;
import com.sun.service.SingerService;

/**
 * 歌手控制层
 * @author maxiao
 *
 */
@RestController
@RequestMapping("/singer")
public class SingerController extends FathController {
	
	@Autowired
	private SingerService singerService;
	
	/**
	 * 查看歌手信息
	 * 
	 * @param singer_id
	 * @return
	 */
	@RequestMapping("/tosinger.do")
	public String tosinger(Integer singer_id, Model model, HttpSession session) {
		Map<String, Object> map = singerService.fingSingerById(singer_id);
		User user = (User) session.getAttribute(USER);
		if (user != null) {
			model.addAttribute(USER, user);
		}
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			model.addAttribute("singer", (Singer) map.get(RET));
			return "page/singer";
		}
		return "page/mainPage";
	}
	
	/**
	 * 加载主页面歌手
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("/addSinger.do")
	public ResultBeanPage addSinger(Integer page) {
		Map<String, Object> map = singerService.findSinger(page);
		ResultBeanPage re = new ResultBeanPage();
		if (TRUE.equals(Tool.dellNull(map.get(FLAG)))) {
			re.setData((List<Singer>) map.get(RET));
		}
		return re;
	}
}
