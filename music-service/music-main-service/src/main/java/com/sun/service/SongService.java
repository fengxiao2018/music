package com.sun.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sun.dao.EstimateMapper;
import com.sun.dao.SongMapper;
import com.sun.dao.SpecialMapper;
import com.sun.lib.Tool;
import com.sun.model.table.Estimate;
import com.sun.model.table.Song;

/**
 * 歌业务类
 * 
 * @author maxiao
 * 
 */
@Service
public class SongService extends FathService {

	@Resource
	private SongMapper songMapper;

	@Resource
	private EstimateMapper estimateMapper;

	@Resource
	private SpecialMapper specialMapper;

	/**
	 * 添加音乐
	 * 
	 * @return
	 */
	public Map<String, Object> saveSong(Song song) {
		if (song == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		song.setPublishtime(Tool.getNewtime());
		songMapper.insert(setSongDellNull(song));
		map.put(FLAG, TRUE);
		map.put(RET, song);
		return map;
	}

	/**
	 * 
	 * @param song
	 * @return
	 */
	private Song setSongDellNull(Song song) {
		String mp3=Tool.dellNull(song.getMp3());
		String mv=Tool.dellNull(song.getMv());
		song.setMp3(mp3);
		song.setMv(mv);
		//m_sign 标志，0表示都有，1只有mp3,2只有mv,4都没
		if ("".equals(song.getmSign())) {
			if (!"".equals(mp3)&&!"".equals(mv)) {
				song.setmSign(0);
			}else if (!"".equals(mp3)&&"".equals(mv)) {
				song.setmSign(1);
			}else if ("".equals(mp3)&&!"".equals(mv)) {
				song.setmSign(2);
			}else{
				song.setmSign(4);
			}
		}else{
			song.setmSign(Tool.parseInt(song.getmSign()));
		}
		song.setSongHead(Tool.dellNull(song.getSongHead()));
		song.setSongName(Tool.dellNull(song.getSongName()));
		song.setRandkingId(Tool.parseInt(song.getRandkingId()));
		song.setIsvipsong(Tool.parseInt(song.getIsvipsong()));
		song.setPublishtime(song.getPublishtime());
		return song;
	}

	/**
	 * 修改音乐
	 * 
	 * @param song
	 * @return
	 */
	public Map<String, Object> updateSong(Song song) {
		if (song == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		songMapper.updateByPrimaryKeySelective(setSongDellNull(song));
		map.put(FLAG, TRUE);
		map.put(RET, song);
		return map;

	}

	/**
	 * 查找mv
	 * 
	 * @param page
	 * @return
	 */
	public Map<String, Object> findSongMVHot(Integer page) {
		if (page == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		List<Song> list = songMapper.findSongMVHot(page * SONGMAINNUM,
				SONGMAINNUM);
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}

	/**
	 * 加载浮动图片
	 * 
	 * @param page
	 * @return
	 */
	public Map<String, Object> findSongMVByPage(Integer page) {
		if (page == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Song song = songMapper.findSongMVByPage(page, 1);
		if (song == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, song);
		}
		return map;
	}

	/**
	 * 加载当前歌曲的评价信息
	 * 
	 * @param page
	 * @return
	 */
	public Map<String, Object> findEstimateBySong(Integer song_id) {
		if (song_id == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		List<Estimate> list = estimateMapper.findBySong(song_id);
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}

	/**
	 * 发表评价
	 * 
	 * @param song_id
	 * @param textarea
	 * @param user_id
	 * @return
	 */
	public Map<String, Object> addEstimate(Integer song_id, String textarea,
			Integer user_id) {
		if (song_id == null || textarea == null || user_id == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Estimate estimate = new Estimate();
		estimate.setAddTime(Tool.getNewtime());
		estimate.setEstimateInfo(textarea);
		estimate.setSongId(song_id);
		estimate.setUserId(user_id);
		estimateMapper.insertSelective(estimate);
		map.put(FLAG, TRUE);
		map.put(RET, estimate);
		return map;
	}

	/**
	 * 加载专辑歌曲
	 * @param page
	 * @return
	 */
	public Map<String, Object> addSpecialSong(Integer special_id) {
		if (special_id == null) {
			throw new RuntimeException(ERRMSG);
		}
		List<Song> list = songMapper.findSpecialSong(special_id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}



	/**
	 * 加载排行榜歌曲
	 * @param page
	 * @param flag
	 * @return
	 */
	public Map<String, Object> addSongBySort(Integer page, Integer flag) {
		if (page == null || flag == null) {
			throw new RuntimeException(ERRMSG);
		}
		String nation = "";
		switch (flag) {
		case 1:
			nation = "";
			break;
		case 2:
			nation = "日韩";
			break;
		case 3:
			nation = "港台";
			break;
		case 4:
			nation = "欧美";
			break;
		case 5:
			nation = "内地";
			break;
		}
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("nation", nation);
		List<Song> list = songMapper.findSongBySort(params,page * SORTNUM,
				SORTNUM);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}

	/**
	 * 加载排行榜音乐的页数
	 * @param flag
	 * @param m_sign 
	 * @return
	 */
	public Map<String, Object> findpageBySort(Integer flag, Integer mSign) {
		if ( flag == null) {
			throw new RuntimeException(ERRMSG);
		}
		String nation = "";
		switch (flag) {
		case 1:
			nation = "";
			break;
		case 2:
			nation = "日韩";
			break;
		case 3:
			nation = "港台";
			break;
		case 4:
			nation = "欧美";
			break;
		case 5:
			nation = "内地";
			break;
		}
		Integer num = songMapper.findpageBySort(nation,mSign);
		Map<String, Object> map = new HashMap<String, Object>();
		if (num == null) {
			map.put(FLAG, FALSE);
		} else {
			Integer numpage=num%SORTNUM==0?num/SORTNUM:num/SORTNUM+1;
			map.put(FLAG, TRUE);
			map.put(RET, numpage);
		}
		return map;
	}
}
