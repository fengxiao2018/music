package com.sun.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sun.dao.SingerMapper;
import com.sun.model.table.Singer;

/**
 * 歌手业务类
 * @author maxiao
 *
 */
@Service
public class SingerService extends FathService{

	@Resource
	private SingerMapper singerMapper;
	
	/**
	 * 加载主页面歌手
	 * @param page 
	 * 
	 * @return
	 */
	public Map<String, Object> findSinger(Integer page) {
		Map<String, String> limit=new HashMap<String, String>();
		limit.put("page", page*SONGMAINNUM+","+SONGMAINNUM);
		List<Singer> list = singerMapper.findSinger(page*SONGMAINNUM,SONGMAINNUM);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}
	
	/**
	 * 查看歌手信息
	 * 
	 * @param singer_id
	 * @return
	 */
	public Map<String, Object> fingSingerById(Integer singer_id) {
		if (singer_id == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Singer singer = singerMapper.selectByPrimaryKey(singer_id);
		if (singer == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, singer);
		}
		return map;
	}
	
}
