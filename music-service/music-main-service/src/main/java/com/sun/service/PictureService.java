package com.sun.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sun.dao.PictureMapper;
import com.sun.model.table.Picture;

@Service
public class PictureService extends FathService {

	@Resource
	private PictureMapper pictureMapper;

	/**
	 * 
	 * @param order
	 * @param pic_category
	 * @param page
	 * @return
	 */
	public List<Picture> findPicture(String order, String pic_category,
			Integer page) {
		Map<String, Object> prams = new HashMap<String, Object>();
		prams.put("order", order);
		List<Picture> list = pictureMapper.findByCategory(prams, pic_category,
				page * PICNUM, PICNUM);
		return list;
	}

	/**
	 * 
	 * @param picture
	 * @return
	 */
	public Picture findPictureBy(Picture picture) {
		if (picture == null) {
			throw new RuntimeException(ERRMSG);
		}
		if (picture.getId() != null) {
			picture = pictureMapper.selectByPrimaryKey(picture.getId());
		} else if (picture.getPicCategory() != null
				&& picture.getPicName() != null) {
			picture = pictureMapper.findByNameAndCategory(
					picture.getPicName(), picture.getPicCategory());
		}
		return picture;
	}
}
