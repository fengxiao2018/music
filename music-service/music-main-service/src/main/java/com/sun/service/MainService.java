package com.sun.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sun.dao.PictureMapper;
import com.sun.dao.SingerMapper;
import com.sun.dao.SongMapper;
import com.sun.model.table.Singer;
import com.sun.model.table.Song;

/**
 * 
 * @author maxiao
 * 
 */
@Service
public class MainService extends FathService {

	@Resource
	private SingerMapper singerMapper;
	
	@Resource
	private SongMapper songMapper;
	
	@Resource
	private PictureMapper pictureMapper;

	/**
	 * 加载主页面歌手
	 * 
	 * @return
	 */
	public Map<String, Object> findSinger() {
		Map<String, String> limit=new HashMap<String, String>();
		limit.put("page", " 0,20 ");
		List<Singer> list = singerMapper.findSinger(0,20);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}

	/**
	 * 查看歌手信息
	 * 
	 * @param singer_id
	 * @return
	 */
	public Map<String, Object> fingSingerById(Integer singerId) {
		if (singerId == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Singer singer = singerMapper.selectByPrimaryKey(singerId);
		if (singer == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, singer);
		}
		return map;
	}

	/**
	 * 加载歌手的歌曲
	 * @param singer_id
	 * @return
	 */
	public Map<String, Object> findSongBySingerId(Integer singerId) {
		if (singerId == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		List<Song> listsong=songMapper.findSongBySingerId(singerId);
		if (listsong == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, listsong);
		}
		return map;
	}
	
	/**
	 * 查看歌曲信息
	 * @param song_id
	 * @return
	 */
	public Map<String, Object> findSongById(Integer song_id) {
		if (song_id == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		Song song=songMapper.selectByPrimaryKey(song_id);
		if (song == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, song);
		}
		return map;
	}
	
	/**
	 * 加载我的音乐  的歌
	 * @param page
	 * @return
	 */
	public Map<String, Object> findSongNew(Integer page) {
		if (page == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		List<Song> song=songMapper.findSongNew(page*SONGMAINNUM,SONGMAINNUM);
		if (song == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, song);
		}
		return map;
	}
	
	/**
	 * 加载音乐的页数
	 * @param page
	 * @return
	 */
	public Map<String, Object> findSongNumPage(Integer mSign) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer num=songMapper.findSongNumPage(mSign);
		if (num == null) {
			map.put(FLAG, FALSE);
		} else {
			Integer numpage=num%SONGMAINNUM==0?num/SONGMAINNUM:num/SONGMAINNUM+1;
			map.put(FLAG, TRUE);
			map.put(RET, numpage);
		}
		return map;
	}


}
