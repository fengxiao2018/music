package com.sun.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sun.dao.SpecialMapper;
import com.sun.model.table.Special;

/**
 * 专辑业务类
 * 
 * @author maxiao
 * 
 */
@Service
public class SpecialService extends FathService {

	@Resource
	private SpecialMapper specialMapper;

	/**
	 * 添加专辑
	 * 
	 * @param special
	 * @return
	 */
	public Map<String, Object> addSpecial(Special special) {
		if (special == null) {
			throw new RuntimeException(ERRMSG);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		specialMapper.insert(special);
		map.put(FLAG, SUCCESS);
		return map;
	}

	/**
	 * 加载专辑
	 * 
	 * @param page
	 * @param flag
	 * @param specialnum
	 * @return
	 */
	public Map<String, Object> findSpecial(Integer page, Integer flag,
			Integer singer_id, Integer specialnum) {
		if (page == null || flag == null || specialnum == null) {
			throw new RuntimeException(ERRMSG);
		}
		String where = "special_hot";
		switch (flag) {
		case 1:
			where = " special_hot,sp_add_time ";
			break;
		case 2:
			where = " special_hot ";
			break;
		case 3:
			where = " sp_add_time ";
			break;
		case 4:
			where = " sp_house ";
			break;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("where", where);
		List<Special> list = specialMapper.findSpecialBy(params, page
				* specialnum, specialnum, singer_id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (list == null) {
			map.put(FLAG, FALSE);
		} else {
			map.put(FLAG, TRUE);
			map.put(RET, list);
		}
		return map;
	}

	/**
	 * 
	 * @param page
	 * @param flag
	 * @param singer_id
	 * @return
	 */
	public Map<String, Object> findSpecial(Integer page, Integer flag,
			Integer singer_id) {
		return findSpecial(page, flag, singer_id, SPECIALNUM);
	}
}
