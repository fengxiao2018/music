package com.sun.service;

import com.sun.model.table.User;

public interface UserService {

	User test(Integer id);

	int addUser(User user);

	int deleteUser(Integer id);

}
