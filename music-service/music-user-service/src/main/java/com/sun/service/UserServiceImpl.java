package com.sun.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.dao.UserMapper;
import com.sun.model.table.User;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UserMapper userMapper;

	@Override
	@Cacheable(value="user",key="'id_'+#id")
	public User test(Integer id) {
		System.out.println("能走到这里去查了库！！");
		return userMapper.selectByPrimaryKey(id);
	}
	
	@Override
	@CachePut(value="user",key="#user.getName()")
	public int addUser(User user) {
		return userMapper.insertSelective(user);
	}
	
	@Override
	@CacheEvict(value="user",key="'id_'+#id")
	public int deleteUser(Integer id) {
		return userMapper.deleteByPrimaryKey(id);
	}
	
}
