package com.sun.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sun.model.beans.result.ResultBean;
import com.sun.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/test/{id}")
	public ResultBean test(@PathVariable Integer id) {
//		return new ResultBean();
		return new ResultBean(userService.test(id));
	}
	
	@RequestMapping("/info")
	public ResultBean info() {
		return new ResultBean(userService.test(1));
	}
	
	@PostMapping("/info1")
	public ResultBean info1(@RequestBody String param) {
		
		System.out.println(param);
		return new ResultBean();
	}

}
