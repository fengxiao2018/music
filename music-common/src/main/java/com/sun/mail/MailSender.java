package com.sun.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Title: MailSender
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author max
 * @date 2018年4月30日 下午11:47:30
 */
//@Component
//@Configuration
public class MailSender {

//	@Autowired
	private JavaMailSender mailSender;

	public void sendSimpleMail(String from, String subject, String test, String... to) throws Exception {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(test);

		mailSender.send(message);
	}

}
