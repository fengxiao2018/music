package com.sun.lib;
/**
 * 常量接口 
 * @author maxiao
 *
 */
public interface SystemConstant {
	/**
	 * 判断变量
	 */
	public static final String FLAG="flag";
	/**
	 * 判断标志
	 */
	public static final String SIGN="sign";
	/**
	 * 验证成功
	 */
	public static final String SUCCESS="success";
	/**
	 * 验证错误
	 */
	public static final String ERROR="error";
	/**
	 * 验证异常
	 */
	public static final String EXCEPTION="exception";
	/**
	 * 信息
	 */
	public static final String MSG="msg";
	/**
	 * 返回变量
	 */
	public static final String RET="ret";
	/**
	 * 正确
	 */
	public static final String TRUE="true";
	/**
	 * 错误
	 */
	public static final String FALSE="false";
	/**
	 * 用户图片显示数 
	 */
	public static final Integer PICNUM=15;
	/**
	 * 主页图片显示数 
	 */
	public static final Integer PICMAINNUM=20;
	/**
	 * 主页歌曲显示数 
	 */
	public static final Integer SONGMAINNUM=10;
	/**
	 * 主页专辑显示数 
	 */
	public static final Integer SPECIALNUM=8;
	/**
	 * 主页排行榜显示数 
	 */
	public static final Integer SORTNUM=8;
	/**
	 * 歌曲标志 MP3
	 */
	public static final Integer SONGMP3=1;
	/**
	 * MV标志 
	 */
	public static final Integer SONGMV=2;
	/**
	 * 错误信息 
	 */
	public static final String ERRMSG="参数不能为空！";
	/**
	 * 是否判断  否 
	 */
	public static final Integer WHETHERNO=0;
	/**
	 * 是否判断  是 
	 */
	public static final Integer WHETHERYSE=1;
	/**
	 * 用户变量
	 */
	public static final String USER="user";
	/**
	 * 用户名不存在
	 */
	public static final String NAMEERROR="nameerror";
	/**
	 * 密码验证错误
	 */
	public static final String PWDERROR="pwderror";
	/**
	 * 用户已登陆
	 */
	public static final String ISLOGIN="islogin";
	/**
	 * 用户已冻结
	 */
	public static final String ISFROZEN="isfrozen";
	/**
	 * 用户不是管理员
	 */
	public static final String ISNOTADMIN="Isnotadmin";
	/**
	 * 实体变量 评价
	 */
	public static final String ESTIMATE="estimate";
	/**
	 * 实体变量 图片
	 */
	public static final String PICTURE="picture";
	/**
	 * 实体变量 歌手
	 */
	public static final String SINGER="singer";
	/**
	 * 实体变量 歌曲
	 */
	public static final String SONG="song";
	/**
	 * 实体变量 专辑
	 */
	public static final String SPECIAL="special";
	/**
	 * 实体变量 管理员 
	 */
	public static final String ADMIN="admin";
	/**
	 * mp3
	 */
	public static final String MP3="mp3";
	/**
	 * mv
	 */
	public static final String MV="mv";
	

}
