package com.sun.lib;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.sun.util.security.Md5Util;

/**
 * 工具类
 * 
 * @author maxiao
 * 
 */
public class Tool implements SystemConstant {

	/**
	 * 取随机名称或id
	 * 
	 * @return
	 */
	public static String getRandomName() {
		String randomName = UUID.randomUUID().toString();
		return randomName;
	}

	/**
	 * 密码验证
	 * 
	 * @param pwd
	 *            密码
	 * @param encryptPwd
	 *            加密密码
	 * @return
	 */
	public static boolean comparePwd(String pwd, String encryptPwd) {
		if (encryptPwd.equals(Md5Util.md5(pwd))) {
			return true;
		}
		return false;
	}

	/**
	 * 空值判断，为null时，返回"";
	 * 
	 * @param obj
	 * @return
	 */
	public static String dellNull(Object obj) {
		String str = "";
		if (obj == null) {
			return str;
		}
		return obj.toString();
	}

	/**
	 * 空值判断，为null时，返回"";
	 * 
	 * @param obj
	 * @return
	 */
	public static Integer parseInt(Object obj) {
		Integer in = 0;
		String str = dellNull(obj);
		if ("".equals(str)) {
			return in;
		}
		try {
			in = Integer.parseInt(str);
		} catch (Exception e) {
			return 0;
		}
		return in;
	}

	/**
	 * 空值判断，为null时，返回0;
	 * 
	 * @param obj
	 * @return
	 */
	public static Long parseLong(Object obj) {
		Long in = (long) 0;
		String str = dellNull(obj);
		if ("".equals(str)) {
			return in;
		}
		try {
			in = Long.parseLong(str);
		} catch (Exception e) {
			return (long) 0;
		}
		return in;
	}

	/**
	 * 空值判断，为null时，返回0;
	 * 
	 * @param obj
	 * @return
	 */
	public static Double parseDouble(Object obj) {
		Double in = (double) 0;
		String str = dellNull(obj);
		if ("".equals(str)) {
			return in;
		}
		try {
			in = Double.parseDouble(str);
		} catch (Exception e) {
			return (double) 0;
		}
		return in;
	}

	/**
	 * 获取当前系统时间
	 * 
	 * @return
	 */
	public static Timestamp getNewtime() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * 获取当前系统时间
	 * 
	 * @return
	 */
	public static Long getNewLongtime() {
		return System.currentTimeMillis();
	}


	/**
	 * 根据文件名加载配制文件
	 * 
	 * @param file
	 *            文件名
	 * @return
	 */
	public static Properties getProperties(String file) {
		Properties props = new Properties();
		try {
			ClassLoader classLoader = Tool.class.getClassLoader();
			InputStream is = classLoader.getResourceAsStream(file);
			props.load(is);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("加载properites文件错误");
		}
		return props;
	}

	/**
	 * 根据key 获取配制file文件中value值，没有匹配的返回本身
	 * 
	 * @param file
	 *            文件名
	 * @param key
	 * @return
	 */
	public static String getPropBykey(String file, String key) {
		String value = "";
		Properties prop = getProperties(file);
		value = prop.getProperty(key);
		if ("".equals(value) || value == null) {
			value = key;
		}
		return value;
	}

	/**
	 * 根据key 获取配制默认（tool.properties）文件中value值，没有匹配的返回本身,
	 * 
	 * @param key
	 * @return
	 */
	public static String getPropBykey(String key) {
		String value = "";
		Properties prop = getProperties("tool.properties");
		value = prop.getProperty(key);
		if ("".equals(value) || value == null) {
			value = key;
		}
		return value;
	}

	/**
	 * 按字节截取字符串
	 * 
	 * @param string
	 *            字符串
	 * @param begin
	 *            起始字节位数
	 * @param end
	 *            终止字节位数
	 * @return
	 */
	public static String subStringByByte(String str, int begin, int end) {
		String beginstr = subStringByByte(str, begin);
		String result = str.substring(beginstr.length() - 1);
		result = subStringByByte(result, end - begin);
		return result;
	}

	/**
	 * 按字节截取字符串
	 * 
	 * @param str
	 *            字符串
	 * @param len
	 *            截取长度
	 * @return
	 */
	public static String subStringByByte(String str, int len) {
		String result = null;
		if (str != null) {
			byte[] a = str.getBytes();
			if (a.length <= len) {
				result = str;
			} else if (len > 0) {
				result = new String(a, 0, len);
				int length = result.length();
				if (str.charAt(length - 1) != result.charAt(length - 1)) {
					if (length < 2) {
						result = null;
					} else {
						result = result.substring(0, length - 1);
					}
				}
				String next = str.substring(result.length() - 1,
						result.length());
				if (!next.equals(getPropBykey("unicode.properties", next))) {
					result += next;
				}
			}
		}
		return result;
	}

	/**
	 * 按指定字节长度截取字符串，返回list集合
	 * 
	 * @param str
	 * @param len
	 *            长度
	 * @return
	 */
	public static List<String> subStringByte(String str, int len) {
		List<String> list = new ArrayList<String>();
		if (str != null) {
			while (str.getBytes().length > len) {
				String result = subStringByByte(str, len);
				list.add(result);
				str = str.substring(result.length());
			}
			list.add(str);
		}
		return list;
	}

	/**
	 * 
	 * @param targetString
	 * @param byteIndex
	 * @return
	 * @throws Exception
	 */
	public static String getSubString(String targetString, int byteIndex)
			throws Exception {
		if (targetString.getBytes("UTF-8").length < byteIndex) {
			throw new Exception("超过长度");
		}
		String temp = targetString;
		for (int i = 0; i < targetString.length(); i++) {
			if (temp.getBytes("UTF-8").length <= byteIndex) {
				break;
			}
			temp = temp.substring(0, temp.length() - 1);
		}
		return temp;
	}


}
