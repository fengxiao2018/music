package com.sun.util.date;

/**
 * <p>
 * Title: DateUtil
 * </p>
 * <p>
 * Description: 日期时间工具类
 * </p>
 * 
 * @author max
 * @date 2018年4月30日 下午6:33:26
 */
public class DateUtil {
	
	/**
	 * 
	 * <p>Title: getNewDateTime</p>  
	 * <p>Description: 获取系统时间/p>  
	 * @return
	 */
	public static Long getNewDateTime(){
		return System.currentTimeMillis();
		
	}
	
	/**
	 * 
	 * <p>Title: getNewDateTimeStr</p>  
	 * <p>Description: 获取系统时间</p>  
	 * @return
	 */
	public static String getNewDateTimeStr(){
		return getNewDateTime().toString();
		
	}

}
