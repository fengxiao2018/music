package com.sun.util.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * <p>
 * Title: RSAResqService
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author max
 * @date 2018年5月13日 下午10:06:18
 */
public class RSAResqService {
	
	private static String PUBLICKEY="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnLajq2x4TatJltFVrn_r9ZNlF8bBUv3GgxnybfFbwKtHAwrqFsVgv-0V8D2g_lYhlFS1StyXHhTdd1FlrTBZSBsWiT1IQVdDr2M81ucQwjL97rhn5od_t7-bSKbRKtakr1nTUlWSZySCR1tnmzPWCnUlVBqrgXQPKV-nlb7XCBwIDAQAB";
	
	private static String PRIVATEKEY="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKctqOrbHhNq0mW0VWuf-v1k2UXxsFS_caDGfJt8VvAq0cDCuoWxWC_7RXwPaD-ViGUVLVK3JceFN13UWWtMFlIGxaJPUhBV0OvYzzW5xDCMv3uuGfmh3-3v5tIptEq1qSvWdNSVZJnJIJHW2ebM9YKdSVUGquBdA8pX6eVvtcIHAgMBAAECgYEAkFMo2ugjxNuZm9fdTrakNhHUST_n3KIR9Jymgo0LePdeca0q3bRGz9V7HSkn9IyhXbK0mA4o_WCSddyW_byIHw8rASljHsPDL1OqsLlg87IgLFK-ldSDgo-d5_br7zsi9nmz5EJJGYbHNQzP-q3IHLnrgaZvnpW_sWlLwjkSrsECQQDPTu6_zbB8eIsMlAG-91YsN8iUnEaCfP1Ba5TWdlF46B_fo5veWfvzOJL0XZi-X_sQde51QmVBiSlVlO-SHvURAkEAznHJO3boe5M0JNfrcrBXpyAeFk0u133W3g-yntUnL4OMZBZ319uRvKxME5L-g2sDSszHRU42-hODo5_BnovllwJAUHALGWchJJrNMTs4ljV4PT4D2Q3uGfGPftPUc4aDREulGmxsLVFy_kOkWab7flrVmuR3ew4hJdWnsSpR7ruOUQJADQwn-bgkLVs6WnHDEbaPc6KWBDbQlRIA-HvpgBxco6Yew1ygKmZYKaKCFGKf71JeHOYgWgL9eoStNzQrmoeg9QJBAIqm4m0q0EO5fk16ULme4yCmyyHZllYQ7wTqJ6NuVKtnOw6VHQo-mhQg8kqXf5NlSswloWGkMjWQnM0j20Hv5bM";

	/**
	 * 
	 * <p>
	 * Title: getResqMap
	 * </p>
	 * <p>
	 * Description: 解密与验签
	 * </p>
	 * 
	 * @param data
	 *            密文
	 * @param sign
	 *            签名
	 * @return
	 */
	public static Map<String, Object> getResqMap(String data, String sign) {
		String privateDecrypt = RSAUtils.privateDecrypt(data, PRIVATEKEY);
		Map<String, Object> resqMap=JSONObject.parseObject(privateDecrypt);
		
		String signs=Md5Util.md5(privateDecrypt);
		if (!sign.equals(signs)) {
			throw new RuntimeException("签名失败！");
		}
		
		return resqMap;
	}
	
	/**
	 * 
	 * <p>Title: setResqMap</p>  
	 * <p>Description: </p>  
	 * @param map
	 * @param publicKey
	 * @return
	 */
	public static Map<String, Object> setResqMap(Map<String, Object> map,String publicKey) {
		Map<String, Object> resqMap=new HashMap<>();
		String data=JSON.toJSONString(map);
		String publicEncrypt = RSAUtils.publicEncrypt(data, publicKey);
		String sign=Md5Util.md5(data);
		resqMap.put("sign", sign);
		resqMap.put("data", publicEncrypt);
		return resqMap;
	}
	
	
	
	public static void main(String[] args) {
		Map<String, Object> map=new HashMap<>();
		map.put("sdf", "sdf");
		map.put("dsfaadf", "fasfa虽地林采欣sdf");
		map.put("sdfasdfwef", "sad要gfwc地wa要ef");
		Map<String, Object> map2=new HashMap<>();
		map2.put("dsfaadf2", "fasfas在df3");
		map2.put("sdfasdfwef2", "sad村fwc夏雨来waef44");
		Map<String, Object> map3=new HashMap<>();
		map3.put("dsfaadf3", "fasfa23s在df3");
		map3.put("sdfasdfwe3", "sad村fwceq夏雨来ewaef44");
		
		List<Map<String, Object>> lists=new ArrayList<>();
		lists.add(map2);
		lists.add(map3);
//		map.put("map2", map2);
		map.put("lists", lists);
		
		Map<String, Object> resqMap = setResqMap(map, PUBLICKEY);
		
		System.out.println("resqMap:"+JSON.toJSONString(resqMap));
		
		String data=(String) resqMap.get("data");
		String sign=(String) resqMap.get("sign");
		
		Map<String, Object> resqMap2 = getResqMap(data, sign);
		
		System.out.println("resqMap2:"+resqMap2);
	}

}
